import java.util.concurrent.Semaphore;

/**
 * Created by user on 5/17/18.
 */
public class ServerThread implements Runnable{
    public Thread thread;
    private Integer[] array;
    private Semaphore mutex;
    private Semaphore empty;
    private Semaphore full;

    public ServerThread(Integer[] array, Semaphore mutex, Semaphore empty, Semaphore full) {
        this.mutex = mutex;
        this.empty = empty;
        this.array = array;
        this.full = full;
        thread = new Thread(this, "WriterThread");
        thread.start();
    }

    @Override
    public void run() {
        while (true){
            try {
                add((int) Math.round(Math.random() * 100));
//                System.out.println("Added a number.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    public boolean add(Integer number) throws InterruptedException {
        boolean success = false;
        mutex.tryAcquire();

        if (empty.availablePermits() < array.length - 1) {
            full.acquire();
            int index = empty.availablePermits();
            array[index] = number;
            empty.release();
            success = true;
        }
        mutex.release();
        return success;
    }
}
