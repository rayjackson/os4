import java.util.concurrent.Semaphore;

public class ClientThread implements Runnable {
    public Thread thread;
    private Integer[] array;
    private Semaphore mutex;
    private Semaphore empty;
    private Semaphore full;

    public ClientThread(Integer[] array, Semaphore mutex, Semaphore empty, Semaphore full) {
        this.array = array;
        this.empty = empty;
        this.full = full;
        this.mutex = mutex;
        thread = new Thread(this, "ReaderThread");
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            Integer elem = null;
            try {
                elem = poll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (elem != null) {
                System.out.println("read " + elem + " by " + thread.getId());
            }
        }
    }

    private Integer poll() throws InterruptedException {
        mutex.tryAcquire();
        Integer elem = null;
        if (empty.availablePermits() > 0) {
            System.out.println(full.availablePermits() + " : " + empty.availablePermits());
            elem = array[full.availablePermits()];
            array[full.availablePermits()] = null;
            empty.acquire();
            full.release();
        }
        mutex.release();
        return elem;
    }
}
