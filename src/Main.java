import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class Main {
    public static final int CLIENT_COUNT = 5;

    public static void main(String[] args) {
        Integer[] array = new Integer[10];
        Semaphore empty = new Semaphore(array.length);
        try {
            empty.acquire(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Semaphore full = new Semaphore(array.length);
        Semaphore mutex = new Semaphore(1);
        new ServerThread(array, mutex, empty, full);
        for (int i = 0; i < CLIENT_COUNT; i++) {
            new ClientThread(array, mutex, empty, full);
        }
    }
}
